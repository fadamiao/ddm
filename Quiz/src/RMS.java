/**
 *
 * @author Fernando A. Damião <me@fadamiao.com>
 * @author André Boutros <andre.boutros@hotmail.com>
 * @author Jonathan Nogueira <jonathan@tec.eti.br>
 * @author Ricardo Heil <ricardoheil@globo.com>
 * @license BSD 3-Clause License
 * 
 */

import javax.microedition.rms.*; 

public abstract class RMS
{
    protected RecordStore rs = null; 

    protected void openRS(String storeName) throws RecordStoreException 
    {
        rs = RecordStore.openRecordStore(storeName, true); 
    }

    protected void closeRS() throws RecordStoreException 
    {
        rs.closeRecordStore(); 
    }

    public void deleteRS(String storeName) throws RecordStoreException
    { 
        RecordStore.deleteRecordStore(storeName);
    }
}
