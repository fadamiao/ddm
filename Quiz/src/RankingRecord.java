/**
 *
 * @author Fernando A. Damião <me@fadamiao.com>
 * @author André Boutros <andre.boutros@hotmail.com>
 * @author Jonathan Nogueira <jonathan@tec.eti.br>
 * @author Ricardo Heil <ricardoheil@globo.com>
 * @license BSD 3-Clause License
 * 
 */

public class RankingRecord
{
    protected String name;
    protected int score;
    protected int time;

    public RankingRecord(String name, int score, int time)
    {
        this.name = name;
        this.score = score;
        this.time = time;
    }

    public String getName()
    {
        return name;
    }

    public int getScore()
    {
        return score;
    }

    public int getTime()
    {
        return time;
    }
}
