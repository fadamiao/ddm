/**
 *
 * @author Fernando A. Damião <me@fadamiao.com>
 * @author André Boutros <andre.boutros@hotmail.com>
 * @author Jonathan Nogueira <jonathan@tec.eti.br>
 * @author Ricardo Heil <ricardoheil@globo.com>
 * @license BSD 3-Clause License
 * 
 */

import javax.microedition.lcdui.Choice;
import javax.microedition.lcdui.ChoiceGroup;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.Gauge;
import javax.microedition.lcdui.StringItem;
import javax.microedition.lcdui.TextField;
import javax.microedition.midlet.*;
import java.util.Vector;
import java.util.Random;

public class Quiz extends MIDlet implements CommandListener
{
    private Display screen;
    private Form frmWelcome;
    private Form frmName;
    private Form frmTheme;
    private Form frmQuestion;
    private Form frmAnswer;
    private Command cmdExit;
    private Command cmdEnter;
    private Command cmdBack;
    private Command cmdNext;
    private Command cmdPlay;
    private Command cmdYes;
    private Command cmdNo;
    private StringItem stiQuestion;
    private ChoiceGroup cgOptions;
    private Gauge progressbar;
    private TextField txtName;
    private String name;
    private int questionNum;
    private int theme;
    private int score;
    private Question[] questions;

    public Quiz()
    {
        screen = Display.getDisplay(this);

        frmWelcome = new Form("GAME - QUIZ");
        frmName = new Form("Dados do Jogador");
        frmTheme = new Form("Tema");
        frmQuestion = new Form("Pergunta");
        frmAnswer = new Form("Resultado Final");

        cmdExit = new Command("Sair", Command.EXIT, 1);
        cmdEnter = new Command("Jogar", Command.SCREEN, 1);
        cmdBack = new Command("Voltar", Command.SCREEN, 2);
        cmdNext = new Command("Próximo", Command.SCREEN, 2);
        cmdPlay = new Command("Jogar", Command.SCREEN, 3);
        cmdYes = new Command("Sim", Command.SCREEN, 5);
        cmdNo = new Command("Não", Command.SCREEN, 5);

        txtName = new TextField("Nome: ", null, 25, TextField.ANY);

        String[] themes = {"Futebol", "Filmes", "Conhecimento Gerais"};
        cgOptions = new ChoiceGroup("Escolha o tema", Choice.EXCLUSIVE, themes, null);

        frmWelcome.append("SEJA BEM \n VINDO AO \n Quiz");
        frmName.append(txtName);
        frmTheme.append(cgOptions);

        frmWelcome.addCommand(cmdExit);
        frmWelcome.addCommand(cmdEnter);
        frmName.addCommand(cmdBack);
        frmName.addCommand(cmdPlay);
        frmTheme.addCommand(cmdBack);
        frmTheme.addCommand(cmdPlay);
        frmQuestion.addCommand(cmdNext);
        frmAnswer.addCommand(cmdYes);
        frmAnswer.addCommand(cmdNo);

        frmWelcome.setCommandListener(this);
        frmName.setCommandListener(this);
        frmTheme.setCommandListener(this);

        questionNum = 0;
    }

    public void startApp()
    {
        screen.setCurrent(frmWelcome);
    }

    public void pauseApp()
    {
    }

    public void destroyApp(boolean unconditional)
    {
    }

    public void commandAction(Command c, Displayable d)
    {
        if (c == cmdExit) {
            destroyApp(true);
            notifyDestroyed();
        } else if (c == cmdEnter) {
            screen.setCurrent(frmName);
        } else if (c == cmdPlay && d == frmName) {
            name = txtName.getString();
            screen.setCurrent(frmTheme);
        } else if (c == cmdBack && d == frmName) {
            screen.setCurrent(frmWelcome);
        } else if (c == cmdPlay && d == frmTheme) {
            theme = cgOptions.getSelectedIndex();
            screen.setCurrent(frmQuestion);
        } else if (c == cmdBack && d == frmTheme) {
            name = "";
            txtName.setString("");
            screen.setCurrent(frmName);
        }
    }

    private void generateQuestion()
    {
        Question quest;
        boolean asked = false;
        Random gen = new Random();
        int indx;
        for (int i = 0; i < 5;i++) {
            while (!asked) {
                indx = gen.nextInt(questions.length);
                if (questions[indx].getTheme() == theme) {
                    if (!questions[indx].isAsked()) {
                        useQuestion(questions[indx]);
                        asked = true;
                    }
                }
                
            }
        }
    }

    public void useQuestion(Question quest)
    {
        frmQuestion.deleteAll();
        quest.ask();
        cgOptions = new ChoiceGroup(quest.getQuestion(), Choice.EXCLUSIVE, quest.getAlternatives(), null);
        frmQuestion.append(cgOptions);
    }
}
