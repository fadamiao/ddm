/**
 *
 * @author Fernando A. Damião <me@fadamiao.com>
 * @author André Boutros <andre.boutros@hotmail.com>
 * @author Jonathan Nogueira <jonathan@tec.eti.br>
 * @author Ricardo Heil <ricardoheil@globo.com>
 * @license BSD 3-Clause License
 * 
 */

public class Question
{
    private String question;
    private String[] alternatives;
    private int theme;
    private boolean asked;

    public Question (int new_theme, String new_question, String[] new_alternatives)
    {
        question = new_question;
        alternatives = new_alternatives;
        theme = new_theme;
        asked = false;
    }

    public void reset()
    {
        asked = false;
    }

    public void ask()
    {
        asked = true;
    }

    public boolean isAsked()
    {
        return asked;
    }

    public int getTheme()
    {
        return theme;
    }

    public String getQuestion()
    {
        return question;
    }

    public String[] getAlternatives()
    {
        return alternatives;
    }
}
