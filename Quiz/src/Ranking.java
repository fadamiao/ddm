/**
 *
 * @author Fernando A. Damião <me@fadamiao.com>
 * @author André Boutros <andre.boutros@hotmail.com>
 * @author Jonathan Nogueira <jonathan@tec.eti.br>
 * @author Ricardo Heil <ricardoheil@globo.com>
 * @license BSD 3-Clause License
 * 
 */

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import javax.microedition.rms.RecordStore;
import javax.microedition.rms.RecordStoreException;

public class Ranking extends RMS
{
    private RecordStore rs = super.rs;

    public Ranking() throws RecordStoreException
    {
        super.openRS("ranking");
    }

    public void insertRecord(String name, int date, int score) throws IOException, RecordStoreException
    {
        ByteArrayOutputStream dataBytes = new ByteArrayOutputStream();
        DataOutputStream streamDataType = new DataOutputStream(dataBytes);
        byte[] record;

        streamDataType.writeUTF(name);
        streamDataType.writeInt(date);
        streamDataType.writeInt(score);
        streamDataType.flush();
        record = dataBytes.toByteArray();

        rs.addRecord(record,0,record.length);

        dataBytes.reset();
        dataBytes.close();
        streamDataType.close();
    }

    public RankingRecord readRecord(int index) throws RecordStoreException, IOException
    { 
        byte[] record = new byte[50];
        ByteArrayInputStream dataBytes = new ByteArrayInputStream(record);
        DataInputStream streamDataType = new DataInputStream(dataBytes);

        rs.getRecord(index, record, 0);

        dataBytes.reset();
        dataBytes.close();
        streamDataType.close();
 
        return new RankingRecord(streamDataType.readUTF(), streamDataType.readInt(), streamDataType.readInt());
    }
    
    public void deleteRecord (int index) throws RecordStoreException
    { 
        rs.deleteRecord(index);
    }
}
