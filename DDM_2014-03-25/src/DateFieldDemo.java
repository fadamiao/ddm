/**
 *
 * @author Fernando A. Damião <me@fadamiao.com>
 * @author André Boutros <andre.boutros@hotmail.com>
 * @author Jonathan Nogueira <jonathan@tec.eti.br>
 * @author Ricardo Heil <ricardoheil@globo.com>
 * @license BSD 3-Clause License
 * @see http://www.rgagnon.com/javadetails/java-0426.html
 * @see http://www.softwareandfinance.com/Java/Number_To_Text.html
 * @see http://stringpool.com/java-program-to-convert-number-to-words/
 * 
 */

import javax.microedition.lcdui.*;
import javax.microedition.midlet.*;
import java.util.*;

public class DateFieldDemo extends MIDlet implements CommandListener, ItemStateListener
{
    private Display objTela;
    private Form frmForm1;
    private Form frmForm2;
    private Form frmForm3;
    private Command cmdExit;
    private Command cmdTela1;
    private Command cmdTela2;
    private Command cmdTela3;
    private DateField dtDateField1;
    private DateField dtDateField2;
    private DateField dtDateField3;
    private StringItem stiIdade;
    private StringItem stiData;

    public DateFieldDemo()
    {
        // Capturando o direito de exibir um objeto na tela
        objTela = Display.getDisplay(this);

        // Criando os 3 formulários que serão exibidos em tela
        frmForm1 = new Form("Formulário 1");
        frmForm2 = new Form("Formulário 2");
        frmForm3 = new Form("Formulário 3");

        // Criando os Commands (Botões) que serão posteriormente
        //  adicionados nos Forms
        cmdExit = new Command("Sair", Command.EXIT, 1);
        cmdTela1 = new Command("Form 1", Command.SCREEN, 1);
        cmdTela2 = new Command("Form 2", Command.SCREEN, 2);
        cmdTela3 = new Command("Form 3", Command.SCREEN, 3);

        // Adicionando os Commands no Formulário 1
        frmForm1.addCommand(cmdExit);
        frmForm1.addCommand(cmdTela2);
        frmForm1.addCommand(cmdTela3);

        // Adicionando os Commands no Formulário 2
        frmForm2.addCommand(cmdExit);
        frmForm2.addCommand(cmdTela1);
        frmForm2.addCommand(cmdTela3);

        // Adicionando os Commands no Formulário 3
        frmForm3.addCommand(cmdExit);
        frmForm3.addCommand(cmdTela1);
        frmForm3.addCommand(cmdTela2);

        // Adicionando Receptores nos Forms
        frmForm1.setCommandListener(this);
        frmForm2.setCommandListener(this);
        frmForm3.setCommandListener(this);

        // Criando os objetos do tipo DateField
        dtDateField1 = new DateField("Data do Form 1", DateField.DATE);
        dtDateField2 = new DateField("Data do Form 2", DateField.DATE_TIME);
        dtDateField3 = new DateField("Data do Form 3", DateField.TIME);

        dtDateField1.setDate(new Date());
        dtDateField2.setDate(new Date());
        dtDateField3.setDate(new Date());

        // Adicionando as caixas de data nos Formulários
        frmForm1.append(dtDateField1);
        frmForm2.append(dtDateField2);
        frmForm3.append(dtDateField3);

        stiIdade = new StringItem("Sua idade é de ", "0 anos");
        frmForm1.append(stiIdade);
        frmForm1.setItemStateListener(this);

        stiData = new StringItem("A data por extenso é ", "data");
        frmForm2.append(stiData);
        frmForm2.setItemStateListener(this);
    }

    public void startApp()
    {
        objTela.setCurrent(frmForm1);
    }
    
    public void pauseApp()
    {
    }
    
    public void destroyApp(boolean unconditional)
    {
    }

    public void commandAction(Command c, Displayable d)
    {
        // Se for o botão sair
        if (c == cmdExit) {
            // Fecha o aplicativo
            destroyApp(true);
            notifyDestroyed();
        }
        // Se for botão para exibir o Formulário 1
          else if (c == cmdTela1) {
            objTela.setCurrent(frmForm1);
        }
        // Se for botão para exibir o Formulário 2
          else if (c == cmdTela2) {
            objTela.setCurrent(frmForm2);
        }
        // Se for botão para exibir o Formulário 3
          else if (c == cmdTela3) {
            objTela.setCurrent(frmForm3);
        }
    }

    private int calcAge(Date dataNascimento)
    {
        // Criando os objetos Clendar para calcular as datas
        Calendar dataAtual = Calendar.getInstance();
        Calendar dataNasc = Calendar.getInstance();

        // Define a data de nascimento fornecida pela usuário
        dataNasc.setTime(dataNascimento);
        // Define a data atual
        dataAtual.setTime(new Date());

        // Define o ano, mês e dia de nascimento
        int anoNasc = dataNasc.get(Calendar.YEAR);
        int mesNasc = dataNasc.get(Calendar.MONTH);
        int diaNasc = dataNasc.get(Calendar.DAY_OF_MONTH);

        // Define o ano, mês e dia de nascimento
        int anoHoje = dataAtual.get(Calendar.YEAR);
        int mesHoje = dataAtual.get(Calendar.MONTH);
        int diaHoje = dataAtual.get(Calendar.DAY_OF_MONTH);

        int idade = 0;

        // Calcula a idade comparando a data atual e a data de nascimento
        if (mesNasc < mesHoje) {
            idade = anoHoje - anoNasc;
        } else if (mesNasc == mesHoje) {
            if (diaNasc > diaHoje) {
                idade = anoHoje - anoNasc - 1;
            } else {
                idade = anoHoje - anoNasc;
            }
        } else {
            idade = anoHoje - anoNasc - 1;
        }

        return idade;
    }

    public void itemStateChanged(Item i)
    {
        if (i == dtDateField1) {
            int idade = calcAge(dtDateField1.getDate());
            stiIdade.setText(String.valueOf(idade) + " anos");
            objTela.setCurrent(frmForm1);
        } else if (i == dtDateField2) {
            String data = escreveDataExtenso(dtDateField2.getDate());
            stiData.setText(data);
        }
    }

    private String escreveDataExtenso(Date d)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(d);

        int ano = cal.get(Calendar.YEAR);
        int mes = cal.get(Calendar.MONTH);
        int dia = cal.get(Calendar.DAY_OF_MONTH);
        int hora = cal.get(Calendar.HOUR_OF_DAY);
        int min = cal.get(Calendar.MINUTE);

        String[] meses = {"Janeiro", "Fevereiro", "Março", "Abril", "Maio",
            "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro",
            "Dezembro"};

        return extenso(dia) + " de " + meses[mes] + " de " + ano + " às " +
                extenso(hora) + " horas e " + extenso(min) + " minutos";
    }

    private String extenso(int i)
    {
        String buffer;
        String[] decimal = {"zero", "um", "dois", "três", "quatro", "cinco",
            "seis", "sete", "oito", "nove", "dez", "onze", "doze", "treze",
            "quatorze", "quinze", "dezesseis", "dezessete", "dezoito", "dezenove"};
        String[] dezena = {"", "", "vinte", "trinta", "quarenta", "cinquenta"};

        if (i > 19) {
            if (i == 20 || i == 30 || i == 40 || i == 50) {
                buffer = dezena[i / 10];
            } else {
                buffer = dezena[i / 10] + " " + decimal[i % 10];
            }
        } else {
            buffer = decimal[i];
        }

        return buffer;
    }
}
