/**
 *
 * @author Fernando A. Damião <me@fadamiao.com>
 * @author André Boutros <andre.boutros@hotmail.com>
 * @author Jonathan Nogueira <jonathan@tec.eti.br>
 * @author Ricardo Heil <ricardoheil@globo.com>
 * @license BSD 3-Clause License
 * 
 */

import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.midlet.*;

public class TratandoEventos extends MIDlet implements CommandListener
{
    private Display objTela;
    private Form frmForm1;
    private Form frmForm2;
    private Form frmForm3;
    private Command cmdExit;
    private Command cmdTela1;
    private Command cmdTela2;
    private Command cmdTela3;

    public TratandoEventos()
    {
        // Capturando o direito de exibir um objeto na tela
        objTela = Display.getDisplay(this);

        // Criando os 3 formulários que serão exibidos em tela
        frmForm1 = new Form("Formulário 1");
        frmForm2 = new Form("Formulário 2");
        frmForm3 = new Form("Formulário 3");

        // Criando os Commands (Botões) que serão posteriormente
        //  adicionados nos Forms
        cmdExit = new Command("Sair", Command.EXIT, 1);
        cmdTela1 = new Command("Form 1", Command.SCREEN, 1);
        cmdTela2 = new Command("Form 2", Command.SCREEN, 2);
        cmdTela3 = new Command("Form 3", Command.SCREEN, 3);

        // Adicionando os Commands no Formulário 1
        frmForm1.addCommand(cmdExit);
        frmForm1.addCommand(cmdTela2);
        frmForm1.addCommand(cmdTela3);

        // Adicionando os Commands no Formulário 2
        frmForm2.addCommand(cmdExit);
        frmForm2.addCommand(cmdTela1);
        frmForm2.addCommand(cmdTela3);

        // Adicionando os Commands no Formulário 3
        frmForm3.addCommand(cmdExit);
        frmForm3.addCommand(cmdTela1);
        frmForm3.addCommand(cmdTela2);

        // Adicionando Receptores nos Forms
        frmForm1.setCommandListener(this);
        frmForm2.setCommandListener(this);
        frmForm3.setCommandListener(this);
    }

    public void startApp()
    {
        objTela.setCurrent(frmForm1);
    }
    
    public void pauseApp()
    {
    }
    
    public void destroyApp(boolean unconditional)
    {
    }

    public void commandAction(Command c, Displayable d)
    {
        // Se for o botão sair
        if (c == cmdExit) {
            // Fecha o aplicativo
            destroyApp(true);
            notifyDestroyed();
        }
        // Se for botão para exibir o Formulário 1
          else if (c == cmdTela1) {
            objTela.setCurrent(frmForm1);
        }
        // Se for botão para exibir o Formulário 2
          else if (c == cmdTela2) {
            objTela.setCurrent(frmForm2);
        }
        // Se for botão para exibir o Formulário 3
          else if (c == cmdTela3) {
            objTela.setCurrent(frmForm3);
        }
    }
}
