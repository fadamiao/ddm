/**
 *
 * @author Fernando A. Damião <me@fadamiao.com>
 * @author André Boutros <andre.boutros@hotmail.com>
 * @author Jonathan Nogueira <jonathan@tec.eti.br>
 * @author Ricardo Heil <ricardoheil@globo.com>
 * @license BSD 3-Clause License
 * 
 */

import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Form;
import javax.microedition.midlet.*;

public class BoaNoiteMidlet extends MIDlet
{
    private Display objTela;
    private Form frmPrincipal;
    private Form frmBomDia;

    public BoaNoiteMidlet()
    {
        objTela = Display.getDisplay(this);
        int numeroCores = objTela.numColors();
        boolean temCores = objTela.isColor();

        frmBomDia = new Form("Bom Dia Turma!!");
        if (temCores) {
            frmPrincipal = new Form("Sua tela tem " + 
                    String.valueOf(numeroCores) + " cores");
        } else {
            frmPrincipal = new Form("Tela sem cores");
        }
    }

    public void startApp()
    {
        objTela.setCurrent(frmPrincipal);
    }

    public void pauseApp()
    {
    }

    public void destroyApp(boolean unconditional)
    {
    }
}
