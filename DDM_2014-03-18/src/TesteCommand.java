/**
 *
 * @author Fernando A. Damião <me@fadamiao.com>
 * @author André Boutros <andre.boutros@hotmail.com>
 * @author Jonathan Nogueira <jonathan@tec.eti.br>
 * @author Ricardo Heil <ricardoheil@globo.com>
 * @license BSD 3-Clause License
 * 
 */

import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.midlet.*;

public class TesteCommand extends MIDlet implements CommandListener
{
    private Display display;
    private Command cmdExit;
    private Form frmMain;

    public TesteCommand()
    {
        display = Display.getDisplay(this);

        cmdExit = new Command("Sair", Command.EXIT, 1);

        frmMain = new Form("Teste");
        frmMain.addCommand(cmdExit);
        frmMain.setCommandListener(this);
        
    }

    public void startApp()
    {
        display.setCurrent(frmMain);
    }
    
    public void pauseApp()
    {
    }
    
    public void destroyApp(boolean unconditional)
    {
    }

    public void commandAction(Command c, Displayable d)
    {
        if (c == cmdExit) {
            destroyApp(true);
            notifyDestroyed();
        }
    }
}
