/**
 *
 * @author Fernando A. Damião <me@fadamiao.com>
 * @author André Boutros <andre.boutros@hotmail.com>
 * @author Jonathan Nogueira <jonathan@tec.eti.br>
 * @author Ricardo Heil <ricardoheil@globo.com>
 * @license BSD 3-Clause License
 * 
 */

import javax.microedition.lcdui.Choice;
import javax.microedition.lcdui.ChoiceGroup;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.Item;
import javax.microedition.lcdui.ItemStateListener;
import javax.microedition.lcdui.StringItem;
import javax.microedition.midlet.*;

public class PedraPapelTesoura extends MIDlet implements CommandListener, ItemStateListener
{
    private Display objTela;
    private Form frmForm1;
    private Form frmForm2;
    private Form frmForm3;
    private Form frmForm4;
    private Form frmForm5;
    private Command cmdExit;
    private Command jogar;
    private Command proximo;
    private Command comparar;
    private Command repetir;
    private Command ajuda;
    private Command voltar;
    private ChoiceGroup cgPPTJ1;
    private ChoiceGroup cgPPTJ2;
    private StringItem stiComeco;
    private StringItem stiJ1;
    private StringItem stiJ2;
    private StringItem stiRES;
    private StringItem stiAjuda;
    private String resJ1;
    private String resJ2;
    private String res;

    private PedraPapelTesoura()
    {
        objTela = Display.getDisplay(this);

        frmForm1 = new Form("Pedra - Papel - Tesoura");
        frmForm2 = new Form("J1 - Escolha");
        frmForm3 = new Form("J2 - Escolha");
        frmForm4 = new Form("Resultado");
        frmForm5 = new Form("Ajuda");

        cmdExit = new Command("Sair", Command.EXIT, 1);
        jogar = new Command("Jogar", Command.SCREEN, 2);
        proximo = new Command("Próximo", Command.SCREEN, 3);
        comparar = new Command("Comparar", Command.SCREEN, 4);
        repetir = new Command("Repetir", Command.SCREEN, 1);
        ajuda = new Command("Ajuda", Command.SCREEN, 5);
        voltar = new Command("Voltar", Command.SCREEN, 1);

        frmForm1.addCommand(cmdExit);
        frmForm1.addCommand(jogar);
        frmForm1.addCommand(ajuda);
        frmForm2.addCommand(proximo);
        frmForm3.addCommand(comparar);
        frmForm4.addCommand(cmdExit);
        frmForm4.addCommand(repetir); 
        frmForm5.addCommand(voltar);
        
        String ppt[] = {"Pedra", "Papel", "Tesoura"};
        cgPPTJ1 = new ChoiceGroup("Escolha sua jogada", Choice.EXCLUSIVE, ppt, null);
        cgPPTJ2 = new ChoiceGroup("Escolha sua jogada", Choice.EXCLUSIVE, ppt, null);

        stiComeco = new StringItem("Entre no jogo", null);
        stiJ1 = new StringItem("J1 Escolheu...", null);
        stiJ2 = new StringItem("J2 Escolheu...", null);
        stiRES = new StringItem(" ", null);
        stiAjuda = new StringItem("Regras do jogo\n" + 
                "Pedra vence Tesoura\n" +
                "Tesoura vence Papel\n" + 
                "Papel vence Pedra\n" + 
                "Valores iguais 'Empatam'"
             , null);

        frmForm1.append(stiComeco);
        frmForm2.append(cgPPTJ1);
        frmForm3.append(cgPPTJ2);
        frmForm4.append(stiJ1);
        frmForm4.append(stiJ2);
        frmForm4.append(stiRES);
        frmForm5.append(stiAjuda);
 
        frmForm1.setCommandListener(this);
        frmForm2.setCommandListener(this);
        frmForm3.setCommandListener(this);
        frmForm4.setCommandListener(this);
        frmForm5.setCommandListener(this);

        frmForm1.setItemStateListener(this);
        frmForm2.setItemStateListener(this);
        frmForm3.setItemStateListener(this);
        frmForm4.setItemStateListener(this);
    }
    
    public void startApp()
    {
        objTela.setCurrent(frmForm1);
    }
    
    public void pauseApp()
    {
    }
    
    public void destroyApp(boolean unconditional)
    {
    }

    public void commandAction(Command c, Displayable d)
    {
        if (c == cmdExit) {
            destroyApp(true);
            notifyDestroyed();
        } else if (c == ajuda) {
            objTela.setCurrent(frmForm5);
        } else if (c == voltar) {
            objTela.setCurrent(frmForm1);
        } else if (c == jogar) {
            objTela.setCurrent(frmForm2);
        } else if (c == repetir) {
            cgPPTJ1.setSelectedIndex(0, true);
            cgPPTJ2.setSelectedIndex(0, true);
            objTela.setCurrent(frmForm1);
        } else if (c == proximo) {
            resJ1 = cgPPTJ1.getString(cgPPTJ1.getSelectedIndex());
            objTela.setCurrent(frmForm3);
        } else if (c == comparar) {
            resJ2 = cgPPTJ2.getString(cgPPTJ2.getSelectedIndex());
            objTela.setCurrent(frmForm4);
            comparar();
        }
    }
    public void itemStateChanged(Item i)
    {
        if (i == cgPPTJ1) {
            resJ1 = cgPPTJ1.getString(cgPPTJ1.getSelectedIndex());
            objTela.setCurrent(frmForm3);
        } else if (i == cgPPTJ2) {
            resJ2 = cgPPTJ2.getString(cgPPTJ2.getSelectedIndex());
            objTela.setCurrent(frmForm4);
            comparar();
        }
    }
    private void comparar() {
        if (resJ1.equals(resJ2)) {
            res = "Empate";
        } else if ("Pedra".equals(resJ1)) {
            if ("Tesoura".equals(resJ2)) {
                res = "J1 Venceu";
            } else if ("Papel".equals(res)) {
                res = "J2 Venceu";
            }
        } else if ("Tesoura".equals(resJ1)){
            if ("Papel".equals(resJ2)) {
                res = "J1 Venceu";
            } else if ("Pedra".equals(resJ2)) {
                res = "J2 Venceu";
            }
        } else if("Papel".equals(resJ1)) {
            if ("Pedra".equals(resJ2)) {
                res = "J1 Venceu";
            } else if ("Tesoura".equals(resJ2)) {
                res = "J2 Venceu";
            }
        }

        stiJ1.setText(resJ1);
        stiJ2.setText(resJ2);
        stiRES.setText(res);
    }
}
